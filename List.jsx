
import React, { Component } from "react";

export default class Planets extends Component {
  state = {
    hasErrors: false,
    planets: {}
  };

  componentDidMount() {
    fetch("https://api.jsonbin.io/b/5fb3b8c94144f562a5ef69e9")
      .then(res => res.json())
      .then(res => this.setState({ planets: res }))
      .catch(() => this.setState({ hasErrors: true }));
  }

  render() {
    return <div>{JSON.stringify(this.state.planets)}</div>;
  }
}