
import React, { useState, useEffect } from "react";

const Planets = () => {
  const [hasError, setErrors] = useState(false);
  const [planets, setPlanets] = useState({});

  useEffect(() =>
    fetch("https://api.jsonbin.io/b/5fb3b8c94144f562a5ef69e9")
      .then(res => res.json())
      .then(res => this.setState({ planets: res }))
      .catch(() => this.setState({ hasErrors: true }))
  );

  return <div>{JSON.stringify(planets)}</div>;
};
export default Planets;
